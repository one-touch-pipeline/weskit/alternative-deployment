#  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#        https://gitlab.com/one-touch-pipeline/weskit/alternative-deployment/-/blob/master/LICENSE
#
#    Authors: The WESkit Team
#

from unittest import TestCase

from weskit_deploy.Config import Config


class TestConfig(TestCase):

    def test___init__(self):
        c1 = Config()
        self.assertEqual(dict(c1), {})

        value = {"a": {"b", 2}}
        c2 = Config(value)
        self.assertDictEqual(dict(c2), value)

    def test___getitem__(self):
        c1 = Config()
        with self.assertRaises(KeyError):
            c1['nonexisting']

        c2 = Config({"a": 1})
        self.assertEqual(c2["a"], 1)

    def test_get(self):
        c1 = Config()
        self.assertEqual(c1.get("a.b.c.d", "hallo"), "hallo")
        self.assertDictEqual(dict(c1), {})

    def test___setitem__(self):
        c1 = Config()
        c1["a.b"] = "c"
        self.assertDictEqual(dict(c1), {"a": {"b": "c"}})

        c1["a.b"] = "d"
        self.assertDictEqual(dict(c1), {"a": {"b": "d"}})

        c1["a.e"] = "f"
        self.assertDictEqual(dict(c1), {"a": {"b": "d", "e": "f"}})

        c1["a"] = "x"
        self.assertDictEqual(dict(c1), {"a": "x"})

    def test_dict_merge(self):
        self.assertDictEqual(Config.dict_merge({}, {}), {})
        self.assertDictEqual(Config.dict_merge({"a": 1}, {}), {"a": 1})
        self.assertDictEqual(Config.dict_merge({}, {"b": 2}), {"b": 2})
        self.assertDictEqual(Config.dict_merge({"a": 1}, {"b": 2}), {"a": 1, "b": 2})
        self.assertDictEqual(Config.dict_merge({"a": 1}, {"a": 2}), {"a": 2})
        self.assertDictEqual(Config.dict_merge({"a": 1}, {"a": {"c": 3}}), {"a": {"c": 3}})
        self.assertDictEqual(Config.dict_merge({"a": 1, "b": 2}, {"a": {"c": 3}}), {"a": {"c": 3}, "b": 2})

        # Ensure the dictionaries are *copies*.
        self.assertIsNot(Config.dict_merge({"a": 1, "b": 2}, {"a": {"c": 3}}), {"a": {"c": 3}, "b": 2})
