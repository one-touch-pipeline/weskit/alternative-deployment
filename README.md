<!--
  ~  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
  ~
  ~ Distributed under the MIT License. Full text at
  ~
  ~      https://gitlab.com/one-touch-pipeline/weskit/alternative-deployment/-/blob/master/LICENSE
  ~
  ~ Authors: The WESkit Team
  -->

# WESkit Swarm Deployment

## Idea

The core idea of deployment approach is to create a directory with the configurations and directories necessary to run WESkit, e.g. a Docker Stack file `compose.yaml`, service configurations in a `configs` directory. You set the parameters, and the rest is autowired. After that you start the stack with the usual Docker Swarm commands.

## Features

* Create a Docker Swarm setup directory
* Optional LSF support
* Optional KeyCloak server (for testing or self-configured)
* Optional development deployment with local code mounted into the container
* Sensible defaults. Configure just the most important parameters to get going
* SSL key setup
* Generate Docker Stack file for production-level deployment

## WESkit

### Targets

The command format is always

```bash
weskit_deploy \
  [--config config.yaml] \
  $target
  [target parameters]
````

The following "targets" are available:

 * `setup`: Create a directory with configurations and other files needed to start the WESkit service stack with Docker Swarm (compose.yaml, etc.)
 * `dump-config`: Read in all configurations and create an effective configuration for deploy_weskit.
 * `up`/`start`: Start the service directly. This includes `setup`
 * `down`/`stop`: Stop the service directly. 
 * `generate-certificate`: Generate self-signed x509 (SSL/TLS) certificates

Just use `--help` with the selected target to learn more.

### How to use it?

You can create a default configuration as follows:

```bash
weskit_deploy dump-config --output outputDirectory > config.yaml
```

The output directory is necessary, because the configuration will be adapted according to the selected output directory (e.g. relative paths). No files will be generated with `dump-config`.

Probably you want to adapt this configuration before using it further. Then, to set up a directory with Docker Swarm configurations you basically do

```bash
weskit_deploy \
  [--config config.yaml] \
  setup \
  --output outputDirectory \
  [--set some.config.variable new-value]
```

The `setup` target will create a number of files and directories in the `--output` directory. The concrete options that you will have to set on the command line depend on your configuration file.

#### Example

```bash
weskit_deploy \
  setup \
  --config config.yaml \
  --output test \
  --set devel_enable true \
  --set weskit.sources path/to/weskit/api/ \
  --set traefik_enable false \
  --set lsf_enable false \
  --set keycloak_enable false \
  --generate-certificate \
  --set weskit.x509.valid_for_days 180
  # --set weskit.x509.private_key_passphrase "passphrase" # not yet supported
```

#### Activating Traefik

If you want to use Traefik you need to set the administrator credentials for login to the dashboard. The credentials are encoded like done by the `htpasswd` tool. You could do

```bash
weskit_deploy \
  setup \
  ...
  --set traefik.admin_credentials "$(htpasswd -nb test test)"
```

For details, see `man htpasswd`.

### Starting the Stack

> NOT IMPLEMENTED: `weskit_deploy up` command.

The current way to go is to use the templated configuration directory directly with Docker Swarm as follows:

```bash
cd $outputDir
export COMPOSE_PROJECT_NAME=projectName
docker stack deploy -c swarm.yaml "$COMPOSE_PROJECT_NAME"
```

> Note that the `COMPOSE_PROJECT_NAME` variable is necessary to work around a characteristic (or bug) of Traefik. Traefik expects the route to the container to go exactly via a network mentioned in the `provider.docker.network`, not via the actual name `${COMPOSE_PROJECT_NAME}_backend. The variable is used to compose the effective network names `${COMPOSE_PROJECT_NAME}_oauth` and `${COMPOSE_PROJECT_NAME}_backend` in the `provider.docker.network` label that is required for automatic route configuration for Traefik. 

### Verify the server is running

#### Without Traefik

If you start the stack without Traefik as reverse proxy, the REST and Keycloak services are available on the ports named in the swarm.yaml config. Unless otherwise defined this will be

* WESkit REST service: https://localhost:5000/ga4ga/wes/v1/:  
* Keycloak interface: https://localhost:8443/auth

E.g. to access the service-info you can do:

```bash
curl \
  --cacert x509/weskit.crt.pem \
  --ipv4 \
  https://localhost:5000/ga4gh/wes/v1/service-info
```

Make sure your cURL proxy settings are correct.

##### Change the hostname

The external hostname by which the service stack is available can be configured with the `server_name` configuration option. The `server_name` should be set to the DNS name with which the server will be available.

#### With Traefik (Reverse Proxy)

If you use Traefik (`traefik_enable=true`), then all services are available via port 8443 (TLS) but are mapped to different routes:

* WESkit REST service: https://localhost/ga4ga/wes/v1/
* Keycloak interface: https://localhost/auth/
* Traefik dashboard: https://localhost/dashboard/
  > NOTE: Do not leave out the terminal '/'. Otherwise, Traefik will not route correctly.

### Directory Management

For WESkit the following design decisions were done

* Only expose relative paths via REST an in the run longs
* Allow for Kerberos-protected mounts
* Allow enforcing the setting of the run-directory via the REST call (using a path that is relative to the `WESKIT_DATA` directory that can easily be mapped to mount-point rooted paths on the client; _via_ `run_dir` tag in the run submission)
* Alternatively, if no `run_dir` tag is enforced, create random run directories.
* All paths communicated via the API should be relative and must not leave the `WESKIT_DATA` directory. This includes `WESKIT_WORKFLOWS`, which is also referenced with a relative path.

For the configuration weskit_deploy provides the following configurations:

* `weskit.container.data`: Container-internal path to data root (=`WESKIT_DATA`)
* `data_volumes` have a `dir` setting that can be an arbitrary path. You can have a mounted directory into which you position `WESKIT_DATA` or the other way around, have multiple data volumes mounted into subdirectories of `WESKIT_DATA`.
* `weskit.container.workflows`: Container-internal path to workflows root (=`WESKIT_WORKFLOWS`). This should be a subdirectory of `WESKIT_DATA`, or it won't be reachable with relative paths from there.

#### Examples

##### Local mount

Mount the path `local/` in the deployment directory as `/data/` into the container

```yaml
data_volumes:
  - name: local
    dir: /data/local
    options:
      driver_opts:
        type: none
        o: bind
        device: ./data
```

The path will be available as `/data/local` in the containers.

The `options` section is simply templated as is into the compose file. Thus, you can (and have) use the structure required for [compose files](https://docs.docker.com/compose/compose-file/#volumes-top-level-element).

##### Local mount with NFS backend

Again use a local bind-mount, but bind a directory `/shared` that may be e.g. a host-level NFS mount to some remote NFS server:

```yaml
data_volumes:
  - name: big_data
    dir: /big-data/shared
    options:
      driver_opts:
        type: none
        o: bind
        device: /big-data
```

##### Mount workflows 

Mount a shared workflows volume into the data directory.

```yaml
data_volumes:
  - name: workflows
    dir: /big-data/workflows
    options:
      driver_opts:
        type: nfs
        o: nfsvers=4.0,rw,sec=krb5,nolock,hard,sync
        device: my.big-data.server:/export/of/data
weskit.container.data: /big-data
weskit.container.wokflows: /big-data/workflows
```

The NFS share `/export/of/data` from the NFS server `my.big-data.server` is mounted into `/big-data/workflows` and used as `WESKIT_WORKFLOWS`.

Note, that you should preserve the relative locations of the data and workflows directories in the containers as they are in the compute-infrastructure. For the execution of the workflows, WESkit uses the path of the workflow file **relative** to the run-directory.

#### Kerberos

If your data is protected in Kerberos guarded mounts, you can configure the containers to access them appropriately. The containers contain the `kinit` etc. tools. The actual Kerberos configuration, however, is mirrored from the OS into the container. This means

  * The `/etc/krb5.conf` file and `/etc/krb5.conf.d` directory are mounted read-only into the container. You need to have configured Kerberos for all Docker hosts in your cluster.
  * The TGT has to be initialized (e.g. ` kinit -r7d -c /tmp/krb5cc_${UID}`) on the host-level, i.e. outside the container. Also ticket renewal has to be done outside (i.e. the `kinit -r`). The TGT file is assumed to be `/tmp/krb5cc_$UID`, where `$UID` has to be the user ID used in the container (set at **build-time** for the image!). The TGT is then simply mirrored into the container as Docker Compose `config` file.
  
The problem with Kerberos, is that the Docker daemon usually runs under another user than WESkit. The access in Kerberos-protected volumes is therefore only possible if a TGT for the Docker daemon is available and authorized. This means, you cannot "bind" mount a subdirectory of a Kerberos-protected share!






Therefore, the advised procedure is 

* Mount the Kerberos-protected volume at the host-level.
* Prepare a TGT for the WESkit user
* Select the paths fromm mount-paths as seen on the local host with bind-mounts as volumes for the `data_volumes` and `workflows_volume`.

For instance,

  1. Your data is available via `big-data.server.org:/path/to/export`. Mount this to `/big-data` on all swarm nodes.
  2. `/big-data/path/to/managed/data` is the path on big-data of the data that shall be accessed by WESkit. Therefore, create a bind-mount to bind it into the containers
     ```yaml
     data_volumes:
       - name: big_data
         subdir: "shared"
         options:
           driver_opts:
             type: none
             o: bind
             device: /big-data/path/to/managed/data
     ```
     This way the path `/big-data/path/to/managed/data` will be accessible as `/data/shared` in the containers.
  3. Your workflows are in a separate installation directory on the same share
     ```yaml
     workflows_volume:
       driver_opts:
         type: none
         o: bind
         device: /big-data/path/to/workflows
     ```
     This will make the workflows directory accessible as `/data/workflows`.

In this case the logs in the run directories will all point to a workflows directory that is not available using this path on
 


Note that you cannot (easily) mount paths within Kerberos-protected filesystems as "bind" mounts by Docker. E.g. if you want to have your workflows in a shared location, nicely visible from inside the container as e.g. `/workflows` directory, you may be tempted to just do a bind mount:

```yaml
    internal:
       {'driver_opts': {
         'type': 'none',
         'o': 'bind', 
         'device': '/mnt/internal'
       }}

    workflows:
       {'driver_opts': {
         'type': 'none', 
         'o': 'bind', 
         'device': '/mnt/internal/path/to/workflows'
       }}
```

If "internal" is protected by Kerberos, then mounting it is no problem in general, because for the file access the TGT is available in the containers. However, Docker runs under a different user that may not have a Kerberos ticket and won't be able to access any path within the mount, e.g. `path/to/workflows/`. So you will not be able to mount the Kerberos share at the host level and mount a subdirectory directly into the stack.

Possible solutions are to have independent Kerberos-protected exports in the NFS server for the `workflows/` and data directory, but only if you have control over the exports.

Finally, you can mount the whole share at the host-level and then mount it into `/data/` in your stack, but access the run directories (`run_dir` tag!) and workflows using relative paths within the `/data/` directory. It may be possible to have `subtree_check` enabled on the NFS server during export, which [might](https://superuser.com/a/894907) allow also mounting subdirectories of exports -- s.th. that would restore the full flexibility for choosing directory names in the containers.

## To-Dos

1. Fix the script installation. Currently, calling is only possible with complex Python CLI referring to `__init__.py`.
2. Consider CI with mypy, bandit, flake8, unit-test
3. Improve security for deployment scripts. How to arrange files and shares such that WESkit cannot just access x509, configs, etc.?
4. Get Traefik to fully working state
   1. Traefik seems not to fully use the SSL certificates: 'level=debug msg="No default certificate, generating one"'. One message of this kind was removed, and setting the TLS and default certificates was necessary to get the certificate to external, but this confusing warning remains.
5. Certificate's key password/encryption is currently turned off.
6. Add tests for specific services (`ServiceStack.ensure_requirements`) 
7. Listen only internally in the overlay network for services. For the Traefik use-case this is done, but for the no-Traefik use-case still all ports are exposed. It should be only REST and Keycloak-Admin. The others may only be turned on in the debug use-case.
8. Remove warnings from services
    1. XFS for MongoDB
    2. keystore for Keycloak: "[org.jboss.as.domain.management.security] (MSC service thread 1-6) WFLYDM0111: Keystore /opt/jboss/keycloak/standalone/configuration/application.keystore not found, it will be auto generated on first use with a self-signed certificate for host localhost"
    3. keycloak: "(ServerService Thread Pool -- 57) WFLYTX0013: The node-identifier attribute on the /subsystem=transactions is set to the default value. This is a danger for environments running multiple servers. Please make sure the attribute value is unique."
9. Implement replicated service nodes (all but workers) and ensure workers won't run on service nodes
10. Allow having multiple manager nodes.
11. Replication MongoDB
12. Replication Redis
13. Replication REST
14. Turn on network encryption (compose file `encrypted: true`)
15. External secrets (no credentials in compose file; probably only with weskit_setup script)
16. Authentication MongoDB
17. Authentication Redis
18. Monitor Redis in container
19. Monitor MongoDB in container
20. Monitor MySQL in container
21. Monitor WESkit REST server
