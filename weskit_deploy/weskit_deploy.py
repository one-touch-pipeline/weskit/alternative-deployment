#!/usr/bin/env python3

#  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#        https://gitlab.com/one-touch-pipeline/weskit/alternative-deployment/-/blob/master/LICENSE
#
#    Authors: The WESkit Team
#

import argparse
import datetime
import logging
import os
import stat
import sys
from pathlib import Path
from typing import List

import yaml
from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID
from jinja2 import Environment, ChoiceLoader, PackageLoader

from Config import Config
from NormalizingValidator import NormalizingValidator
from ServiceStack import ServiceStack
from SwarmService import SwarmService

env = Environment(
    loader=ChoiceLoader([
        PackageLoader('resources', 'stack'),
        PackageLoader('resources', 'configs')
    ])
)

logger = logging.getLogger(__name__)


def parseArgs(args):

    def add_config_argument(parser):
        parser.add_argument('--set', dest="config_values", nargs=2, action='append')

    parser = argparse.\
        ArgumentParser(description='Manage a WESkit service stack deployment is Docker Swarm.',
                       epilog="The effective configuration is the union of the default, the `-c`, and the `--set` parameters, in increasing order of priority. The effective configuration is validated and possibly a validation error is issued.")  # noqa
    parser.add_argument('-c', '--config', dest='config_file', action='store',
                        default="config.yaml",
                        help='The name of the configuration file to use.')

    subparsers = parser.add_subparsers(dest="mode")

    setup_parser = subparsers.\
        add_parser("setup", aliases=["init"],
                   description="Setup default directories and configurations.")
    setup_parser.add_argument("-o", "--output", dest="output_directory", action="store",
                              help="Output directory to create the configuration files in.")
    setup_parser.add_argument("-u", "--update", dest="update", action="store_true",
                              help="Allow update/override for an existing output directory.")
    setup_parser.add_argument("-g", "--generate-certificate", dest="generate_certificate",
                              action="store_true",
                              help="Generate a x509 certificates in the x509/ subdirectory.")
    add_config_argument(setup_parser)

    dump_config_parser = subparsers.\
        add_parser("dump-config",
                   description="Dump the effective configuration.")
    dump_config_parser.add_argument("-o", "--output", dest="output_directory", action="store",
                                    help="Output directory to create the configuration files in.")
    dump_config_parser.add_argument("-g", "--generate-certificate", dest="generate_certificate",
                                    action="store_true",
                                    help="Generate a x509 certificates in the x509/ subdirectory.")
    add_config_argument(dump_config_parser)

    start_parser = subparsers.\
        add_parser("up", aliases=["start"],
                   description="Start the service stack.")
    start_parser.add_argument('-a', '--additional-compose', dest='compose_files', action='append',
                              default=[],
                              help='Additional compose files. Added to the stack after the default compose file.')   # noqa
    add_config_argument(start_parser)

    stop_parser = subparsers.\
        add_parser("down", aliases=["stop"],
                   description="Stop the service stack.")
    add_config_argument(stop_parser)

    options = parser.parse_args(args)

    return options


def workflows_relative_to_data(config):
    container_workflow_path = Path(config["weskit.container.workflows"])
    container_data_path = Path(config["weskit.container.data"])
    if not container_workflow_path.is_relative_to(container_data_path):
        raise ValueError("weskit.container.workflows has to be relative to weskit.container.data: "
                         f"Got {container_data_path} and {container_workflow_path}")


def generate_certificates(outdir: Path, prefix: str, config: Config):
    """
    Generate a set of self-signed TLS certificates in the output directory. Key and the certificate
    are stored into the directory in PEM and DER formats. The filenames will be of the pattern

        outdir/prefix.{key,crt}.{pem,der}

    See https://cryptography.io/en/latest/x509/tutorial/
    """
    def write_file(file, content):
        # Write our key to disk for safe keeping
        if os.path.isfile(file):
            print("Overwriting %s" % file, file=sys.stderr)
        else:
            print("Creating %s" % file, file=sys.stderr)

        with open(file, "wb") as f:
            f.write(content)

    assert workflows_relative_to_data(config)

    os.makedirs(outdir, exist_ok=True)

    full_prefix = outdir / prefix
    config["weskit.secrets.key_pem"] = {"file": f"{full_prefix}.key.pem"}
    config["weskit.secrets.crt_pem"] = {"file": f"{full_prefix}.crt.pem"}
    config["weskit.secrets.crt_der"] = {"file": f"{full_prefix}.crt.der"}

    # Generate our key
    key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=config["weskit.x509.bits"],
    )

    write_file(os.path.join(outdir, config["weskit.secrets.key_pem"]["file"]),
               key.private_bytes(
                   encoding=serialization.Encoding.PEM,
                   format=serialization.PrivateFormat.TraditionalOpenSSL,
                   encryption_algorithm=serialization.NoEncryption()
               ))
    # TODO BestAvailableEncryption(bytes(config["weskit.x509.private_key_passphrase"], "utf-8"))
    #      Somehow the password needs to go into the containers!
    write_file(f"{full_prefix}.key.der",
               key.private_bytes(
                   encoding=serialization.Encoding.DER,
                   format=serialization.PrivateFormat.TraditionalOpenSSL,
                   encryption_algorithm=serialization.NoEncryption()
               ))

    # Various details about who we are. For a self-signed certificate the
    # subject and issuer are always the same.
    subject = issuer = x509.Name([
            x509.NameAttribute(NameOID.COUNTRY_NAME,
                               config["weskit.x509.country_name"]),
            x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME,
                               config["weskit.x509.state_or_province_name"]),
            x509.NameAttribute(NameOID.LOCALITY_NAME,
                               config["weskit.x509.locality_name"]),
            x509.NameAttribute(NameOID.ORGANIZATION_NAME,
                               config["weskit.x509.organization_name"]),
            x509.NameAttribute(NameOID.COMMON_NAME,
                               config["weskit.x509.common_name"]),
        ])
    cert = x509.CertificateBuilder().subject_name(
            subject
        ).issuer_name(
            issuer
        ).public_key(
            key.public_key()
        ).serial_number(
            x509.random_serial_number()
        ).not_valid_before(
            datetime.datetime.utcnow()
        ).not_valid_after(
            # Our certificate will be valid for 10 days
            datetime.datetime.utcnow() + datetime.timedelta(
                days=int(config["weskit.x509.valid_for_days"]))
        ).add_extension(
            x509.SubjectAlternativeName(list(map(lambda n: x509.DNSName(n),
                                                 config["weskit.x509.subject_alternative_names"]))),
            critical=False,
            # Sign our certificate with our private key
        ).sign(key, hashes.SHA256())

    # Write our certificate out to disk.
    write_file(os.path.join(outdir, config["weskit.secrets.crt_pem.file"]),
               cert.public_bytes(serialization.Encoding.PEM))
    write_file(os.path.join(outdir, config["weskit.secrets.crt_der.file"]),
               cert.public_bytes(serialization.Encoding.DER))


def create_from_template(path, name, kwargs):
    with open(os.path.join(path, name), "w") as f:
        template = env.get_template(name)
        print(template.render(**kwargs), file=f)


def setup_docker_compose_dir(outdir, update):
    if os.path.isdir(outdir):
        if update:
            print("Overriding %s" % outdir, file=sys.stderr)
        else:
            print("Refusing to override %s" % outdir, file=sys.stderr)
            exit(8)
    os.makedirs(outdir, exist_ok=update)


def get_api_source_dir(outdir, config: Config):
    if config["devel_enable"]:
        if os.path.isdir(config["weskit.sources"]):
            print("Using WESkit sources at '%s" % config["weskit.sources"],
                  file=sys.stderr)
            rel_source_path = \
                os.path.relpath(os.path.abspath(config["weskit.sources"]),
                                os.path.abspath(outdir))
            return rel_source_path
        else:
            print("Invalid WESkit sources directory: %s" %
                  config["weskit.sources"],
                  file=sys.stderr)
            exit(7)


def absolute_path(pathname: str, default_base_dir: str) -> str:
    """
    If the path is absolute, use it. Otherwise, prepend the default_base_dir.
    If the default_base_dir itself is relative, it is interpreted relative to the current working
    directory.
    """
    if not os.path.isabs(default_base_dir):
        default_base_dir = os.path.normpath(os.path.join(os.getcwd(), default_base_dir))

    if os.path.isabs(pathname):
        effective_dir = os.path.normpath(pathname)
    else:
        effective_dir = os.path.normpath(os.path.join(default_base_dir, pathname))
        print("Interpreting %s relative to %s directory." % (pathname, default_base_dir))
    return effective_dir


def setup_directory(name, default_base_dir, pathname, container_gid, group_permissions) -> None:
    path = absolute_path(pathname, default_base_dir)
    if os.path.isdir(path):
        print("Using existing directory '%s'" % path,
              file=sys.stderr)
        current_gid = os.stat(path).st_gid
        if current_gid != container_gid:
            print("'%s' directory may have wrong access rights for group ID %s" %
                  (name, container_gid),
                  file=sys.stderr)
    else:
        print("Creating %s directory '%s'" % (name, path),
              file=sys.stderr)
        os.makedirs(path, exist_ok=True)

        print("Setting owner to GID %s used in container and g+rwx: %s" %
              (container_gid, path))
        os.chown(path, os.getuid(), container_gid)
        os.chmod(path, group_permissions)


def setup_data_directory(default_base_dir, dir, container_gid):
    setup_directory("data", default_base_dir, dir, container_gid,
                    stat.S_IRWXG | stat.S_IRWXU)


def create_output_dir(options, service_stack: ServiceStack):
    """
    Create default directories and configurations from the effective configuration. The
    configuration object is cloned and the modified clone is returned.
    """

    config = service_stack.config.copy()
    outdir = Path(options.output_directory)

    try:
        setup_docker_compose_dir(outdir, options.update)

        config['weskit.sources'] = get_api_source_dir(outdir, config)

        if options.generate_certificate:
            generate_certificates(outdir / "x509", "weskit", config)

        # Store effective configuration
        with open(os.path.join(outdir, "stack-config.yaml"), "w") as f:
            print(yaml.dump(dict(config)), file=f)

        # Create configuration files from templates
        create_from_template(outdir, "swarm.yaml", dict(config))

        config_dir = os.path.join(outdir, "configs")
        os.makedirs(config_dir, exist_ok=True)
        create_from_template(config_dir, "weskit.yaml", dict(config))
        create_from_template(config_dir, "mongodb.yaml", dict(config))
        create_from_template(config_dir, "log-config.yaml", dict(config))
        create_from_template(config_dir, "uwsgi.ini", dict(config))
        create_from_template(config_dir, "redis.conf", dict(config))
        create_from_template(config_dir, "keycloak.sql", dict(config))
        create_from_template(config_dir, "traefik.yaml", dict(config))

        def driver_opts_o(vol: dict) -> List[str]:
            return vol["options"]["driver_opts"].get("o", "").split(",")

        print("Setting up mounts in the container ...", file=sys.stderr)
        data_volumes = list(filter(lambda v: "bind" in driver_opts_o(v),
                                   config["data_volumes"]))
        for mount in data_volumes:
            setup_data_directory(outdir,
                                 mount["options"]["driver_opts"]["device"],
                                 config["weskit.gid"])

    except OSError as e:
        logger.error(str(e), exc_info=e)
        print("An error occurred: %s" % str(e), file=sys.stderr)
        exit(6)

    return config


def dump_config(config: Config) -> None:
    """
    Print the Configuration as YAML.
    """
    print(yaml.dump(dict(config)))


def ensure_absolute_local_volume_paths(config: Config, outdir: str) -> None:
    """
    Local volumes must be provided as absolute paths. If relative paths are encountered,
    they are interpreted relative to the output directory (just like in the local directory
    setup later).
    """
    for vol in config["data_volumes"]:
        driver_opts = vol["options"]["driver_opts"]
        if driver_opts["o"] == "bind":
            if not os.path.isabs(driver_opts["device"]):
                driver_opts["device"] = absolute_path(driver_opts["device"], outdir)


def main(args):
    parsed_args = parseArgs(args)

    with open("resources/validation.yaml", "r") as validation_file:
        validator = NormalizingValidator(yaml.safe_load(validation_file))

    try:
        with open(parsed_args.config_file, "r") as config_file:
            custom_config = yaml.safe_load(config_file)
    except OSError as e:
        print("Could not read '%s': %s" % (parsed_args.config_file, str(e)),
              file=sys.stderr)
        exit(3)

    with open("resources/default_config.yaml", "r") as default_config_file:
        default_config = yaml.safe_load(default_config_file)

    # Compose the configuration object from various sources
    config = Config(validator, parsed_args.output_directory, default_config)
    config.update(custom_config)
    config.update(parsed_args.config_values)
    config.validate()

    ensure_absolute_local_volume_paths(config, parsed_args.output_directory)

    if parsed_args.mode == "dump-config":
        dump_config(config)
    else:
        service_definition = ServiceStack(config)
        if parsed_args.mode == "setup":
            create_output_dir(parsed_args, service_definition)
        elif parsed_args.mode in ["start", "up"]:
            swarm_service = SwarmService(service_definition)
            swarm_service.start(parsed_args.compose_files)
        elif parsed_args.mode in ["stop", "down"]:
            swarm_service = SwarmService(service_definition)
            swarm_service.stop()
        else:
            print("Mode '%s' does not exist." % parsed_args.mode, file=sys.stderr)
            exit(4)
