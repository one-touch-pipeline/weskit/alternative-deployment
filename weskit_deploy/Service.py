#  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#        https://gitlab.com/one-touch-pipeline/weskit/alternative-deployment/-/blob/master/LICENSE
#
#    Authors: The WESkit Team
#

from abc import ABCMeta
from typing import List, Dict, Optional

from Config import Config


class Service(metaclass=ABCMeta):

    def __init__(self, config: Config):
        self.config = config

    def _config_map(self):
        return {}

    def compose_file(self) -> Optional[str]:
        return None


class Base(Service):

    def __init__(self, config):
        super().__init__(config)

    def compose_file(self) -> Optional[str]:
        return "resources/compose/base.yaml"


class Development(Service):

    def __init__(self, config):
        super().__init__(config)

    def compose_file(self) -> Optional[str]:
        return "resources/compose/devel.yaml"


class LSF(Service):

    def __init__(self, config):
        super().__init__(config)

    def compose_file(self) -> Optional[str]:
        return "resources/compose/lsf.yaml"


class Keycloak(Service):

    def __init__(self, config):
        super().__init__(config)

    def compose_file(self) -> Optional[str]:
        return "resources/compose/keycloak.yaml"


class Traefik(Service):

    def __init__(self, config):
        super().__init__(config)

    def compose_file(self) -> Optional[str]:
        return "resources/compose/traefik.yaml"
