#  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#        https://gitlab.com/one-touch-pipeline/weskit/alternative-deployment/-/blob/master/LICENSE
#
#    Authors: The WESkit Team
#

import logging
from functools import reduce
from typing import List
import subprocess

from ServiceStack import ServiceStack

logger = logging.getLogger(__name__)


class SwarmService:

    def __init__(self, service_stack: ServiceStack):
        self._service_stack = service_stack

    def up(self, additional_compose_files: List[str]):
        pass
        # env = self._service_definition.compose_environment()
        # command = ["docker", "stack", "deploy"]
        # command += reduce(lambda a, l: a + l,
        #                   map(lambda f: ["-c", f],
        #                       self._service_definition.compose_files() +
        #                       additional_compose_files),
        #                   [])
        # command += [self._service_definition.name]
        # logger.info("Executing: %s with environment %s" % (command, env))
        # result = subprocess.run(command, env=env)
        # if result.returncode != 0:
        #     logger.error("\n".join(["Failed starting service stack",
        #                             *map(lambda v: "export %s='%s'" % (v[0], v[1]),
        #                                  env.items()),
        #                             " ".join(command)]))
        # return result.returncode != 0

    def start(self, additional_compose_files: List[str]):
        return self.up(additional_compose_files)

    def down(self):
        command = ["docker", "stack", "rm", self._service_stack.name]
        logger.info("Executing: %s with environment %s" % (command, {}))
        result = subprocess.run(command)
        if result.returncode != 0:
            logger.error("\n".join(["Failed stopping service stack",
                                    "command=%s" % command]))
        return result.returncode != 0

    def stop(self):
        return self.down()
