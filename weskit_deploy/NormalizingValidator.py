#  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#        https://gitlab.com/one-touch-pipeline/weskit/alternative-deployment/-/blob/master/LICENSE
#
#    Authors: The WESkit Team
#

import sys
from typing import Any

from cerberus import Validator


class NormalizingValidator(Validator):

    def __init__(self, *args, purge_unknown=True, **kwargs):
        super(NormalizingValidator, self).__init__(*args, purge_unknown=purge_unknown, **kwargs)

    def _normalize_coerce_integer(self, value: str) -> int:
        return int(value)

    def _normalize_coerce_boolean(self, value: str) -> bool:
        if isinstance(value, str):
            if value.lower() == "true":
                return True
            elif value.lower() == "false":
                return False
            else:
                raise ValueError("Cannot cast to boolean: '%s'" % value)
        else:
            return bool(value)

    def _normalize_coerce_string(self, value: Any) -> str:
        return str(value)

    def _normalize_default_setter_name(self, doc) -> str:
        return doc["name"]
