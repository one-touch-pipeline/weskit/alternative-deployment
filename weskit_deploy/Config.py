#  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#        https://gitlab.com/one-touch-pipeline/weskit/alternative-deployment/-/blob/master/LICENSE
#
#    Authors: The WESkit Team
#

from copy import deepcopy
from functools import reduce
from itertools import groupby
from typing import List, Optional, Union

from cerberus import Validator


class Config:

    def __init__(self, validator: Validator,
                 output_directory: str,
                 config: Optional[dict] = None):
        if config is None:
            self._config = {}
        else:
            self._config = config
        self._validator = validator
        self.output_directory = output_directory

    def copy(self):
        copy = Config(self._validator, self.output_directory)
        copy.update(self._config)
        return copy

    def synchronize_server_name(self) -> None:
        """
        The server name is the name under which the server will be available externally. This name
        will be set for all services (REST, Keycloak, etc.) from the `server_name` configuration.
        This name will also be added to the TLS key.
        """
        server_name = self._config["server_name"]
        self._config["weskit"]["x509"]["common_name"] = server_name
        self._config["weskit"]["x509"]["subject_alternative_names"].append(server_name)

    def normalize(self):
        """
        Use Cerberus's normalization feature.

        Unfortunately the default_setter approach for the volume "subdir" did not work. So,
        we set the values here :-(
        """

        norm_config = self._validator.normalized(self._config)
        if norm_config is None:
            raise RuntimeError("Normalization error")
        self._config = norm_config

        self.synchronize_server_name()

        normalized_data_volumes = []
        for vol in self._config["data_volumes"]:
            if "subdir" not in vol:
                vol["subdir"] = vol["name"]
            normalized_data_volumes.append(vol)
        self._config["data_volumes"] = normalized_data_volumes

    def validate(self) -> None:
        self._validator.validate(dict(self))
        validation_errors = self._validator.errors

        # Now normalize the configuration. This is needed for the next validation step.
        self.normalize()

        # All volumes should have different mount points (that are reused in possibly multiple
        # containers). Not sure how to validate this with Cerberus. So for simplicity's sake,
        # manual validation:
        def subdir(d: dict):
            return d["subdir"]
        data_volumes = sorted(self["data_volumes"], key=subdir)
        for k, g in groupby(data_volumes, subdir):
            if len(list(g)) > 1:
                validation_errors.append(
                    "Multiple data_volumes map to same container-directory '%s'" % k
                )

        if len(validation_errors) > 0:
            raise RuntimeError("Error validating the configuration: %s" % ", ".
                               join(validation_errors))

    @staticmethod
    def dict_merge(this: dict, other: dict) -> dict:
        if not isinstance(this, dict):
            result = deepcopy(other)
        elif isinstance(other, dict):
            result = {}
            for k in set(this.keys()).union(set(other.keys())):
                if k in this.keys() and k in other.keys():
                    result[k] = Config.dict_merge(this[k], other[k])
                elif k in this.keys():
                    result[k] = this[k]
                else:
                    result[k] = deepcopy(other[k])
        else:
            result = deepcopy(other)
        return result

    def update(self, other: Union[dict, list]) -> None:
        """
        Merge the given configuration dictionary into this Config. This Config will be modified.
        """
        if isinstance(other, dict):
            self._config = Config.dict_merge(self._config, other)
        elif isinstance(other, list):
            for pair in other:
                self[pair[0]] = pair[1]

    def __iter__(self):
        for (k, v) in self._config.items():
            yield k, v

    def __getitem__(self, item):
        """
        Retrieve a configuration value. The key may be a dot-separated list. Throws KeyError,
        if any path-element is not existent.
        """
        keys = item.split(".")
        return reduce(dict.__getitem__, keys, self._config)

    def get(self, key: str, default=None):
        """
        Get value with default None or any other default. The Config is not modified.
        """
        keys = key.split(".")
        if len(keys) == 0:
            raise KeyError()
        return reduce(lambda d, k: d.get(k, {}),
                      keys[0:len(keys)-1],
                      self._config).\
            get(keys[-1], default)

    @staticmethod
    def _set(data: dict, keys: List[str], value):
        if len(keys) == 0:
            # Note: Whatever is in data, it will be lost here.
            return value
        if len(keys) == 1:
            data[keys[0]] = value
            return data
        if len(keys) > 0:
            k = keys[0]
            if data.__class__ is not dict:
                raise KeyError()
            new_dict = data.get(k, {})
            new_value = Config._set(new_dict, keys[1:], value)
            data[k] = new_value
            return data

    def __setitem__(self, key: str, value) -> None:
        """
        Set the value. If the value does not exist, create the necessary nested dicts.
        """
        self._config = Config._set(self._config, key.split("."), value)
