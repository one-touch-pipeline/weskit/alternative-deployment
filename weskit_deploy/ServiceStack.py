#  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#        https://gitlab.com/one-touch-pipeline/weskit/alternative-deployment/-/blob/master/LICENSE
#
#    Authors: The WESkit Team
#

from functools import reduce
from typing import List, Dict

from Config import Config
from Service import Service


class ServiceStack:

    def __init__(self, config: Config):
        self.config = config

    @property
    def name(self) -> str:
        return self.config["name"]
